<?php
namespace app\index\controller;

use app\helper\controller\Agent;
use think\Controller;
use think\Request;

class Index extends Controller
{
    public function index()
    {
        $host = $_SERVER['HTTP_HOST'];
        if (!empty($host)) {
            $bindhost = \app\index\model\Index::get(['bindhost'=>$host]);
            if (!empty($bindhost)) {
                header("Location: ". $_W['siteroot'] . 'app/index.php?i='.$bindhost['uniacid'].'&t='.$bindhost['id']);
                exit;
            }
        }
        $os = Agent::deviceType();
        if($os == 'mobile' && (!empty(Request::instance()->get('i')) || !empty($_SERVER['QUERY_STRING']))) {
            header('Location: ./app/index.php?' . $_SERVER['QUERY_STRING']);
            exit;
        } else {
            var_dump(11);exit;
            header('Location: ./web/index.php?' . $_SERVER['QUERY_STRING']);
            exit;
        }
    }
}
