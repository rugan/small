<?php

namespace app\Index\controller;

use think\Controller;
use think\Request;

class Login extends Controller
{

    public function index()
    {

    }

    public function _login()
    {
        $setting_sms_sign = setting_load('site_sms_sign');
        $status = !empty($setting_sms_sign['site_sms_sign']['status']) ? $setting_sms_sign['site_sms_sign']['status'] : '';
        if (!empty($status)) {
            user_expire_notice();
        }
        if (empty($_GPC['login_type'])) {
            $_GPC['login_type'] = 'system';
        }

        $member = OAuth2Client::create($_GPC['login_type'], $_W['setting']['thirdlogin'][$_GPC['login_type']]['appid'], $_W['setting']['thirdlogin'][$_GPC['login_type']]['appsecret'])->we7user();

        if (is_error($member)) {
            itoast($member['message'], url('user/login'), '');
        }

        $record = user_single($member);
        if (!empty($record)) {
            if ($record['status'] == USER_STATUS_CHECK || $record['status'] == USER_STATUS_BAN) {
                itoast('您的账号正在审核或是已经被系统禁止，请联系网站管理员解决！', url('user/login'), '');
            }
            $_W['uid'] = $record['uid'];
            $_W['isfounder'] = user_is_founder($record['uid']);
            $_W['user'] = $record;

            if (empty($_W['isfounder']) || user_is_vice_founder()) {
                if (!empty($record['endtime']) && $record['endtime'] < TIMESTAMP) {
                    itoast('您的账号有效期限已过，请联系网站管理员解决！', '', '');
                }
            }
            if (!empty($_W['siteclose']) && empty($_W['isfounder'])) {
                itoast('站点已关闭，关闭原因：' . $_W['setting']['copyright']['reason'], '', '');
            }
            $cookie = array();
            $cookie['uid'] = $record['uid'];
            $cookie['lastvisit'] = $record['lastvisit'];
            $cookie['lastip'] = $record['lastip'];
            $cookie['hash'] = md5($record['password'] . $record['salt']);
            $session = authcode(json_encode($cookie), 'encode');
            isetcookie('__session', $session, !empty($_GPC['rember']) ? 7 * 86400 : 0, true);
            $status = array();
            $status['uid'] = $record['uid'];
            $status['lastvisit'] = TIMESTAMP;
            $status['lastip'] = CLIENT_IP;
            user_update($status);

            if (empty($forward)) {
                $forward = user_login_forward($_GPC['forward']);
            }

            if ($record['uid'] != $_GPC['__uid']) {
                isetcookie('__uniacid', '', -7 * 86400);
                isetcookie('__uid', '', -7 * 86400);
            }
            $failed = pdo_get('users_failed_login', array('username' => trim($_GPC['username']), 'ip' => CLIENT_IP));
            pdo_delete('users_failed_login', array('id' => $failed['id']));
            itoast("欢迎回来，{$record['username']}。", $forward, 'success');
        } else {
            if (empty($failed)) {
                pdo_insert('users_failed_login', array('ip' => CLIENT_IP, 'username' => trim($_GPC['username']), 'count' => '1', 'lastupdate' => TIMESTAMP));
            } else {
                pdo_update('users_failed_login', array('count' => $failed['count'] + 1, 'lastupdate' => TIMESTAMP), array('id' => $failed['id']));
            }
            itoast('登录失败，请检查您输入的用户名和密码！', '', '');
        }
    }
}
