<?php

namespace app\helper\controller;


class Cache
{

    public static function cache_load($key, $unserialize = false) {
        static $_cache;
        if (!empty($_cache[$key])) {
            return $_cache[$key];
        }
        $data = $_cache[$key] = cache_read($key);
        if ($key == 'setting') {
            $setting = $data;
            return $setting;
        } elseif ($key == 'modules') {
            $modules = $data;
            return $modules;
        } elseif ($key == 'module_receive_enable' && empty($data)) {
            cache_build_module_subscribe_type();
            return cache_read($key);
        } else {
            return $unserialize ? iunserializer($data) : $data;
        }
    }

}
