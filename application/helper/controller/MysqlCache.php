<?php

namespace app\helper\controller;

class MysqlCache
{
    public static function cache_read($key)
    {
        $cachedata = pdo_getcolumn('core_cache', array('key' => $key), 'value');
        if (empty($cachedata)) {
            return '';
        }
        $cachedata = iunserializer($cachedata);
        if (is_array($cachedata) && !empty($cachedata['expire']) && !empty($cachedata['data'])) {
            if ($cachedata['expire'] > time()) {
                return $cachedata['data'];
            } else {
                return '';
            }
        } else {
            return $cachedata;
        }
    }
}
