<?php

namespace app\helper\controller;

class Setting
{
    public static function setting_load($key = '')
    {
        $cachekey = "setting";
        $settings = cache_load($cachekey);
        if (empty($settings)) {
            $settings = pdo_fetchall('SELECT * FROM ' . tablename('core_settings'), array(), 'key');
            if (is_array($settings)) {
                foreach ($settings as $k => &$v) {
                    $settings[$k] = iunserializer($v['value']);
                }
            }
            cache_write($cachekey, $settings);
        }
        if (!is_array($_W['setting'])) {
            $_W['setting'] = array();
        }
        $_W['setting'] = array_merge($_W['setting'], $settings);
        if (!empty($key)) {
            return array($key => $settings[$key]);
        } else {
            return $settings;
        }
    }
}
